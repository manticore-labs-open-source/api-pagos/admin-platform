module.exports = {
  rootDir: "src",
  transform: {
    "^.+\\.(j|t)sx?$": "babel-jest",
  },
  moduleNameMapper: {
    "\\.(css)$": "identity-obj-proxy",
    "\\.(png)$": "identity-obj-proxy",
  },
  setupFilesAfterEnv: [
    "../node_modules/@testing-library/jest-dom/dist/index.js",
  ],
  coverageThreshold: {
    global: {
      branches: 19,
      functions: 33,
      lines: 26,
      statements: 26,
    },
  },
  coverageDirectory: "../coverage",
  collectCoverageFrom: ["./**/*.{ts,tsx}"],
  setupFiles: ["../jest.global.jsx"],
};
