import React from "react";
import { DialogTitle, IconButton, Theme, Typography } from "@material-ui/core";
import { X } from "react-feather";
import { makeStyles } from "@material-ui/core/styles";

export interface AppDialogTitleProps {
  id: string;
  title?: string;
  titleVariant?: "h1" | "h2" | "h3" | "h4" | "h5" | "h6";
  onClose: (value: string) => void;
}

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
}));

export const AppDialogTitle: React.FC<AppDialogTitleProps> = (
  props: AppDialogTitleProps
) => {
  const classes = useStyles();

  return (
    <DialogTitle disableTypography className={classes.root} id={props.id}>
      {props.title !== undefined ? (
        <Typography variant={props.titleVariant}>{props.title}</Typography>
      ) : null}
      <IconButton
        aria-label="close"
        className={classes.closeButton}
        onClick={() => props.onClose("cancel")}
      >
        <X size={20} />
      </IconButton>
    </DialogTitle>
  );
};
