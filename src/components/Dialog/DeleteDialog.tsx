import React from "react";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
} from "@material-ui/core";
import { AppDialogTitle } from "./AppDialogTitle";

export interface DeleteDialogProps {
  open: boolean;
  onClose: (value: string) => void;
}

export const DeleteDialog: React.FC<DeleteDialogProps> = (
  props: DeleteDialogProps
) => {
  const handleClose = (value: string) => {
    props.onClose(value);
  };

  return (
    <Dialog onClose={() => props.onClose("cancel")} open={props.open}>
      <AppDialogTitle
        id="simple-dialog-title"
        title="Eliminar Gateway"
        titleVariant="h6"
        onClose={() => props.onClose("cancel")}
      />
      <DialogContent>
        <DialogContentText>
          Está seguro que desea eliminar el registro?
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={() => handleClose("cancel")} color="primary">
          Cancelar
        </Button>
        <Button
          onClick={() => handleClose("delete")}
          variant="contained"
          color="primary"
        >
          Eliminar
        </Button>
      </DialogActions>
    </Dialog>
  );
};
