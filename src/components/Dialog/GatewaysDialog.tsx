import React, { useEffect, useState } from "react";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  Grid,
  Select,
  TextField,
} from "@material-ui/core";
import { Catalog, Gateway } from "../../store/reducer";
import { AppDialogTitle } from "./AppDialogTitle";
import { set } from "lodash";

export interface GatewaysDialogProps {
  open: boolean;
  onClose: (
    value: string,
    gateway: Gateway,
    action: "create" | "update"
  ) => void;
  providers: Catalog[];
  gateway?: Gateway;
}

export const GatewaysDialog: React.FC<GatewaysDialogProps> = (
  props: GatewaysDialogProps
) => {
  const [gatewayState, setGatewayState] = useState<Gateway>({
    name: "",
    id: "",
    credential: {
      publicCredential: "",
      privateCredential: "",
    },
    type: "sandbox",
    provider: "kushki",
    status: "",
    created: 0,
  });
  const [nameError, setNameError] = useState<boolean>(false);
  const [publicError, setPublicError] = useState<boolean>(false);
  const [privateError, setPrivateError] = useState<boolean>(false);

  useEffect(() => {
    console.log(props.providers);
    if (props.gateway) setGatewayState({ ...props.gateway });
    else
      setGatewayState({
        name: "",
        id: "",
        credential: {
          publicCredential: "",
          privateCredential: "",
        },
        type: "sandbox",
        status: "",
        created: 0,
        provider: "kushki",
      });
  }, [props.gateway]);

  const handleClose = (value: string): void => {
    if (checkErrors() && value === "save") return;

    const action = props.gateway === undefined ? "create" : "update";

    setNameError(false);
    setPublicError(false);
    setPrivateError(false);
    props.onClose(value, { ...gatewayState }, action);
  };

  const checkErrors = (): boolean => {
    let has_errors = false;

    if (gatewayState.name.length === 0) {
      setNameError(true);
      has_errors = true;
    }
    if (gatewayState.credential.publicCredential.length === 0) {
      setPublicError(true);
      has_errors = true;
    }
    if (gatewayState.credential.privateCredential.length === 0) {
      setPrivateError(true);
      has_errors = true;
    }

    return has_errors;
  };

  const handleGatewayState = (path: string, value: string): void => {
    const new_gateway_state = { ...gatewayState };

    set(new_gateway_state, path, value);

    setGatewayState({ ...new_gateway_state });
  };

  return (
    <Dialog onClose={() => handleClose("cancel")} open={props.open} fullWidth>
      <AppDialogTitle
        id="simple-dialog-title"
        title={gatewayState.id !== "" ? "Editar Gateway" : "Crear Gateway"}
        titleVariant="h4"
        onClose={handleClose}
      />
      <DialogContent>
        <Grid container spacing={2} direction="column">
          <Grid item>
            <TextField
              fullWidth
              variant="outlined"
              label="Nombre"
              value={gatewayState.name}
              onChange={(e) => {
                handleGatewayState("name", e.target.value);
                setNameError(e.target.value.length === 0);
              }}
              error={nameError}
              helperText={
                nameError ? "El campo nombre no puede ser vacio." : ""
              }
            />
          </Grid>
          <Grid item>
            <Select
              native
              label="Tipo"
              disabled={props.gateway !== undefined}
              inputProps={{
                name: "gateway-type",
                id: "gateway-type-label",
              }}
              value={gatewayState.type}
              onChange={(e) =>
                handleGatewayState("type", e.target.value as string)
              }
              variant="outlined"
              fullWidth
            >
              <option value="sandbox">Sandbox</option>
              <option value="production">Producción</option>
            </Select>
          </Grid>
          <Grid item>
            <Select
              native
              label="Proveedor"
              disabled={props.gateway !== undefined}
              inputProps={{
                name: "gateway-provider",
                id: "gateway-provider-label",
              }}
              value={gatewayState.provider}
              onChange={(e) =>
                handleGatewayState("provider", e.target.value as string)
              }
              variant="outlined"
              fullWidth
            >
              {[
                { value: "kushki", name: "Kushki" },
                { value: "stripe", name: "Stripe" },
              ].map((provider: Catalog, index: number) => (
                <option key={index} value={provider.value}>
                  {provider.name}
                </option>
              ))}
            </Select>
          </Grid>
          <Grid item>
            <TextField
              fullWidth
              variant="outlined"
              label="Credencial Pública"
              value={gatewayState.credential.publicCredential}
              onChange={(e) => {
                handleGatewayState(
                  "credential.publicCredential",
                  e.target.value
                );
                setPublicError(e.target.value.length === 0);
              }}
              error={publicError}
              helperText={
                publicError
                  ? "El campo credencial pública no puede ser vacio."
                  : ""
              }
            />
          </Grid>
          <Grid item>
            <TextField
              fullWidth
              variant="outlined"
              label="Credencial Privada"
              value={gatewayState.credential.privateCredential}
              onChange={(e) => {
                handleGatewayState(
                  "credential.privateCredential",
                  e.target.value
                );
                setPrivateError(e.target.value.length === 0);
              }}
              error={privateError}
              helperText={
                privateError
                  ? "El campo credencial privada no puede ser vacio."
                  : ""
              }
            />
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions>
        <Button
          onClick={() => handleClose("save")}
          variant="contained"
          color="primary"
        >
          Guardar
        </Button>
      </DialogActions>
    </Dialog>
  );
};
