import React from "react";
import { Dialog, DialogContent, Grid, Typography } from "@material-ui/core";
import { AppDialogTitle } from "./AppDialogTitle";
import { Transaction } from "../../store/reducer";
import { get } from "lodash";
import Highlight from "react-highlight.js";
import Inactive from "../../assets/images/inactive.png";
import Active from "../../assets/images/active.png";
import { format } from "date-fns";

export interface AppDialogProps {
  open: boolean;
  onClose: () => void;
  transaction: Transaction;
}

export const TransactionsDialog: React.FC<AppDialogProps> = (
  props: AppDialogProps
) => {
  return (
    <Dialog
      onClose={() => props.onClose()}
      open={props.open}
      fullWidth
      maxWidth="lg"
    >
      <AppDialogTitle
        id="simple-dialog-title"
        title={`Transacción ${get(props, "transaction.id", "")}`}
        titleVariant="h5"
        onClose={() => props.onClose()}
      />
      <DialogContent>
        <Grid container spacing={2}>
          <Grid item xs={12} sm container>
            <Grid item xs container direction="column" spacing={2}>
              <Grid item xs>
                <Typography variant="body2" color="textSecondary">
                  Token:
                </Typography>
                <Typography variant="body2">
                  {get(props, "transaction.token.id", "")}
                </Typography>
              </Grid>
            </Grid>
            <Grid item xs container direction="column" spacing={2}>
              <Grid item xs>
                <Typography variant="body2" color="textSecondary">
                  Referencia:
                </Typography>
                <Typography variant="body2">
                  {get(props, "transaction.transactionReference", "")}
                </Typography>
              </Grid>
            </Grid>
            <Grid item xs container direction="column" spacing={2}>
              <Grid item xs>
                <Typography variant="body2" color="textSecondary">
                  ID Gateway:
                </Typography>
                <Typography variant="body2">
                  {get(props, "transaction.gatewayId", "")}
                </Typography>
              </Grid>
            </Grid>
            <Grid item xs container direction="column" spacing={2}>
              <Grid item xs>
                <Typography variant="body2" color="textSecondary">
                  Proveedor:
                </Typography>
                <Typography variant="body2">
                  {get(props, "transaction.provider", "")}
                </Typography>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Grid container spacing={2}>
          <Grid item xs={12} sm container>
            <Grid item xs container direction="column" spacing={2}>
              <Grid item xs>
                <Typography variant="body2" color="textSecondary">
                  Moneda:
                </Typography>
                <Typography variant="body2">
                  {get(props, "transaction.currency", "").toUpperCase()}
                </Typography>
              </Grid>
            </Grid>
            <Grid item xs container direction="column" spacing={2}>
              <Grid item xs>
                <Typography variant="body2" color="textSecondary">
                  Monto:
                </Typography>
                <Typography variant="body2">
                  $ {get(props, "transaction.amount", "")}
                </Typography>
              </Grid>
            </Grid>
            <Grid item xs container direction="column" spacing={2}>
              <Grid item xs>
                <Typography variant="body2" color="textSecondary">
                  Tipo:
                </Typography>
                <Typography variant="body2">
                  {get(props, "transaction.type", "")}
                </Typography>
              </Grid>
            </Grid>
            <Grid item xs container direction="column" spacing={2}>
              <Grid item xs>
                <Typography variant="body2" color="textSecondary">
                  Estado:
                </Typography>
                <Typography variant="body2">
                  <img
                    src={
                      get(props, "transaction.status", "") === "declined"
                        ? Inactive
                        : Active
                    }
                    width={"10px"}
                    style={{ marginRight: "5px" }}
                    alt={get(props, "transaction.status", "")}
                  />
                  {get(props, "transaction.status", "") === "declined"
                    ? "Declinada"
                    : "Aprobada"}
                </Typography>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Grid container spacing={2}>
          <Grid item xs={12} sm container>
            <Grid item xs container direction="column" spacing={2}>
              <Grid item xs>
                <Typography variant="body2" color="textSecondary">
                  Bin:
                </Typography>
                <Typography variant="body2">
                  {get(props, "transaction.token.card.bin", "")}
                </Typography>
              </Grid>
            </Grid>
            <Grid item xs container direction="column" spacing={2}>
              <Grid item xs>
                <Typography variant="body2" color="textSecondary">
                  Últimos 4 Dígitos:
                </Typography>
                <Typography variant="body2">
                  {get(props, "transaction.token.card.last4", "")}
                </Typography>
              </Grid>
            </Grid>
            <Grid item xs container direction="column" spacing={2}>
              <Grid item xs>
                <Typography variant="body2" color="textSecondary">
                  Marca:
                </Typography>
                <Typography variant="body2">
                  {get(props, "transaction.token.card.brand", "")}
                </Typography>
              </Grid>
            </Grid>
            <Grid item xs container direction="column" spacing={2}>
              <Grid item xs>
                <Typography variant="body2" color="textSecondary">
                  Tipo de Tarjeta:
                </Typography>
                <Typography variant="body2">
                  {get(props, "transaction.token.card.type", "") === "debit"
                    ? "Débito"
                    : "Crédito"}
                </Typography>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Grid container spacing={2}>
          <Grid item xs={12} sm container>
            <Grid item xs container direction="column" spacing={2}>
              <Grid item xs>
                <Typography variant="body2" color="textSecondary">
                  Descripción:
                </Typography>
                <Typography variant="body2">
                  {get(props, "transaction.description", "")}
                </Typography>
              </Grid>
            </Grid>
            <Grid item xs container direction="column" spacing={2}>
              <Grid item xs>
                <Typography variant="body2" color="textSecondary">
                  Fecha:
                </Typography>
                <Typography variant="body2">
                  {format(
                    get(props, "transaction.created", 0),
                    "yyyy-MM-dd HH:mm:ss"
                  )}
                </Typography>
              </Grid>
            </Grid>
            <Grid item xs container direction="column" spacing={2}>
              <Grid item xs>
                <Typography variant="body2" color="textSecondary">
                  Código de Respuesta:
                </Typography>
                <Typography variant="body2">
                  {get(props, "transaction.responseCode", "")}
                </Typography>
              </Grid>
            </Grid>
            <Grid item xs container direction="column" spacing={2}>
              <Grid item xs>
                <Typography variant="body2" color="textSecondary">
                  Mensaje de Respuesta:
                </Typography>
                <Typography variant="body2">
                  {get(props, "transaction.responseText", "")}
                </Typography>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Grid container spacing={2}>
          <Grid item xs={12} sm container>
            <Grid item xs container direction="column" spacing={2}>
              <Grid item xs>
                <Typography variant="body2" color="textSecondary">
                  Metadata:
                </Typography>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Highlight language="javascript">
          {JSON.stringify(get(props, "transaction.metadata", {}), undefined, 4)}
        </Highlight>
        <Grid container spacing={2}>
          <Grid item xs={12} sm container>
            <Grid item xs container direction="column" spacing={2}>
              <Grid item xs>
                <Typography variant="body2" color="textSecondary">
                  Detalles:
                </Typography>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Highlight language="javascript">
          {JSON.stringify(get(props, "transaction.details", {}), undefined, 4)}
        </Highlight>
      </DialogContent>
    </Dialog>
  );
};
