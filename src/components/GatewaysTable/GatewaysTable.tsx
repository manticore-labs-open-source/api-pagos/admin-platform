import React from "react";
import {
  Box,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from "@material-ui/core";
import { Gateway } from "../../store/reducer";
import { format } from "date-fns";
import { GatewaysTableMenu } from "./GatewaysTableMenu";
import Active from "../../assets/images/active.png";
import Inactive from "../../assets/images/inactive.png";

interface GatewaysTableProps {
  gateways: Gateway[];
  onDelete: (id: string) => void;
  onUpdate: (gateway: Gateway) => void;
  onChangeStatus: (gateway: Gateway, newStatus: string) => void;
}

export const GatewaysTable: React.FC<GatewaysTableProps> = (
  props: GatewaysTableProps
) => {
  return (
    <React.Fragment>
      <TableContainer>
        <Table aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>ID</TableCell>
              <TableCell>Nombre</TableCell>
              <TableCell>Fecha de Creación</TableCell>
              <TableCell>Estado</TableCell>
              <TableCell>Tipo</TableCell>
              <TableCell>Proveedor</TableCell>
              <TableCell>Credenciales</TableCell>
              <TableCell />
            </TableRow>
          </TableHead>
          <TableBody>
            {props.gateways.map((gateway: Gateway, index: number) => (
              <TableRow key={index}>
                <TableCell component="th" scope="row">
                  {gateway.id}
                </TableCell>
                <TableCell>{gateway.name}</TableCell>
                <TableCell>
                  <Typography variant="body2">
                    {format(gateway.created, "yyyy-MM-dd")}
                  </Typography>
                  <Typography variant="subtitle2" color={"textSecondary"}>
                    {format(gateway.created, "HH:mm:ss")}
                  </Typography>
                </TableCell>
                <TableCell>
                  <img
                    src={gateway.status === "active" ? Active : Inactive}
                    width={"10px"}
                    style={{ marginRight: "5px" }}
                    alt={gateway.status}
                  />
                  {gateway.status === "active" ? "Activa" : "Inactiva"}
                </TableCell>
                <TableCell>
                  {gateway.type === "sandbox" ? "Sandbox" : "Producción"}
                </TableCell>
                <TableCell>{gateway.provider}</TableCell>
                <TableCell>
                  <Typography variant="subtitle2" color={"textSecondary"}>
                    Pública
                  </Typography>
                  <Typography variant="body2">
                    {gateway.credential.publicCredential.slice(0, 10)}
                    {gateway.credential.publicCredential.length > 10
                      ? "****"
                      : ""}
                  </Typography>
                  <Box mt={1} />
                  <Typography variant="subtitle2" color={"textSecondary"}>
                    Privada
                  </Typography>
                  <Typography variant="body2">
                    {gateway.credential.privateCredential.slice(0, 10)}
                    {gateway.credential.privateCredential.length > 10
                      ? "****"
                      : ""}
                  </Typography>
                </TableCell>
                <TableCell>
                  <GatewaysTableMenu
                    gateway={gateway}
                    onDelete={props.onDelete}
                    onUpdate={props.onUpdate}
                    onChangeStatus={props.onChangeStatus}
                  />
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </React.Fragment>
  );
};
