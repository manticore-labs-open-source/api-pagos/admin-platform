import React, { useState } from "react";
import { Button, Menu, MenuItem } from "@material-ui/core";
import { MoreHorizontal } from "react-feather";
import { makeStyles, Theme } from "@material-ui/core/styles";
import { Gateway } from "../../store/reducer";

export interface GatewaysTableMenuProps {
  gateway: Gateway;
  onDelete: (id: string) => void;
  onUpdate: (gateway: Gateway) => void;
  onChangeStatus: (gateway: Gateway, newStatus: string) => void;
}

const useStyles = makeStyles((theme: Theme) => ({
  menuRed: {
    color: theme.palette.error.main,
  },
  buttonWidth: {
    minWidth: "unset",
  },
}));

export const GatewaysTableMenu: React.FC<GatewaysTableMenuProps> = (
  props: GatewaysTableMenuProps
) => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);

  const handleCloseEl = (): void => {
    setAnchorEl(null);
  };

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>): void => {
    setAnchorEl(event.currentTarget);
  };

  const handleOnDelete = (): void => {
    setAnchorEl(null);
    props.onDelete(props.gateway.id);
  };

  const handleOnUpdate = (): void => {
    setAnchorEl(null);
    props.onUpdate(props.gateway);
  };

  const handleChangeStatus = (): void => {
    setAnchorEl(null);
    props.onChangeStatus(
      props.gateway,
      props.gateway.status === "active" ? "inactive" : "active"
    );
  };

  return (
    <React.Fragment>
      <Button
        className={classes.buttonWidth}
        size="small"
        variant="outlined"
        color="primary"
        onClick={handleClick}
      >
        <MoreHorizontal size={20} />
      </Button>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleCloseEl}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center",
        }}
        elevation={1}
        getContentAnchorEl={null}
      >
        <MenuItem onClick={handleOnUpdate}>Editar</MenuItem>
        <MenuItem onClick={handleChangeStatus}>
          {props.gateway.status !== "active" ? "Activar" : "Desactivar"}
        </MenuItem>
        <MenuItem onClick={handleOnDelete} className={classes.menuRed}>
          Eliminar
        </MenuItem>
      </Menu>
    </React.Fragment>
  );
};
