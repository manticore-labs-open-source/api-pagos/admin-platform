import React from "react";
import { Button, Grid } from "@material-ui/core";
import { ChevronRight, ChevronLeft } from "react-feather";
import { makeStyles } from "@material-ui/core/styles";

export interface ListPaginationProps {
  page: number;
  disableNext: boolean;
  disablePrevious: boolean;
  handleChangePage: (newPage: number) => void;
}

const useStyles = makeStyles(() => ({
  root: {
    padding: "8px 27px 8px 20px",
  },
  buttonIconPagination: {
    textTransform: "none",
  },
  marginButton: {
    marginTop: -6,
    display: "flex",
  },
  typographyLabelPagination: {
    marginTop: "auto",
    marginBottom: "auto",
    marginRight: 11,
  },
}));

export const ListPagination: React.FC<ListPaginationProps> = (
  props: ListPaginationProps
) => {
  const classes = useStyles(props);

  const handlePage = (action: string): void => {
    let new_page: number;

    if (action === "next") new_page = props.page + 1;
    else new_page = props.page - 1;

    props.handleChangePage(new_page);
  };

  return (
    <React.Fragment>
      <div className={classes.root}>
        <Grid container>
          <Grid
            item
            xs={2}
            lg={8}
            xl={8}
            md={4}
            sm={6}
            className={classes.marginButton}
          />
          <Grid
            item
            xs={10}
            lg={4}
            xl={4}
            md={8}
            sm={6}
            container
            alignItems="flex-start"
            justify="flex-end"
            direction="row"
          >
            <div>
              <Button
                className={classes.buttonIconPagination}
                startIcon={<ChevronLeft size={25} />}
                disabled={props.disablePrevious}
                onClick={() => handlePage("previous")}
              >
                Anterior
              </Button>
            </div>
            <div>
              <Button
                className={classes.buttonIconPagination}
                endIcon={<ChevronRight size={25} />}
                disabled={props.disableNext}
                onClick={() => handlePage("next")}
              >
                Siguiente
              </Button>
            </div>
          </Grid>
        </Grid>
      </div>
    </React.Fragment>
  );
};
