import React from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@material-ui/core";
import { Skeleton } from "@material-ui/lab";

interface SkeletonTableProps {
  headCells: number;
  bodyCells: number;
  rows: number;
}

export const SkeletonTable: React.FC<SkeletonTableProps> = (
  props: SkeletonTableProps
) => {
  return (
    <React.Fragment>
      <TableContainer>
        <Table aria-label="simple table">
          <TableHead>
            <TableRow>
              {[...Array(props.headCells)].map((_: string, index: number) => (
                <TableCell key={index}>
                  <Skeleton variant="text" />
                </TableCell>
              ))}
              <TableCell />
            </TableRow>
          </TableHead>
          <TableBody>
            {[...Array(props.rows)].map((_: string, index: number) => (
              <TableRow key={index}>
                {[...Array(props.bodyCells)].map(
                  (_: string, innerIndex: number) => (
                    <TableCell key={innerIndex}>
                      <Skeleton variant="text" />
                    </TableCell>
                  )
                )}
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </React.Fragment>
  );
};
