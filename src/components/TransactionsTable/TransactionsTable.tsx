import React from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from "@material-ui/core";
import { Transaction } from "../../store/reducer";
import { format } from "date-fns";
import { get } from "lodash";
import Active from "../../assets/images/active.png";
import Inactive from "../../assets/images/inactive.png";

interface TransactionsTableProps {
  transactions: Transaction[];
  selectedIndex: number;
  setSelectedIndex: (index: number) => void;
  setOpenExistModal: (open: boolean) => void;
}

export const TransactionsTable: React.FC<TransactionsTableProps> = (
  props: TransactionsTableProps
) => {
  return (
    <React.Fragment>
      <TableContainer>
        <Table aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>ID</TableCell>
              <TableCell>Cliente</TableCell>
              <TableCell>Fecha</TableCell>
              <TableCell>Estado</TableCell>
              <TableCell>Tipo</TableCell>
              <TableCell>Monto</TableCell>
              <TableCell>Proveedor</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {props.transactions.map(
              (transaction: Transaction, index: number) => (
                <TableRow
                  key={index}
                  hover
                  selected={props.selectedIndex === index}
                  onClick={() => {
                    props.setSelectedIndex(index);
                    props.setOpenExistModal(true);
                  }}
                >
                  <TableCell component="th" scope="row">
                    {transaction.id}
                  </TableCell>
                  <TableCell>{get(transaction, "token.card.name", "")}</TableCell>
                  <TableCell>
                    <Typography variant="body2">
                      {format(transaction.created, "yyyy-MM-dd")}
                    </Typography>
                    <Typography variant="subtitle2" color={"textSecondary"}>
                      {format(transaction.created, "HH:mm:ss")}
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <img
                      src={
                        transaction.status === "declined" ? Inactive : Active
                      }
                      width={"10px"}
                      style={{ marginRight: "5px" }}
                      alt={transaction.status}
                    />
                    {transaction.status === "declined"
                      ? "Declinada"
                      : "Aprobada"}
                  </TableCell>
                  <TableCell>{transaction.type}</TableCell>
                  <TableCell>{transaction.amount}</TableCell>
                  <TableCell>{transaction.provider}</TableCell>
                </TableRow>
              )
            )}
          </TableBody>
        </Table>
      </TableContainer>
    </React.Fragment>
  );
};
