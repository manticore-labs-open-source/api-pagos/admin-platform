import React, { useEffect, useState } from "react";
import {
  Avatar,
  Box,
  Button,
  Grid,
  Toolbar,
  List,
  Typography,
  Divider,
  ListItem,
} from "@material-ui/core";
import { Route, Switch, useHistory } from "react-router-dom";
import { createStyles, Theme, makeStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import AppBar from "@material-ui/core/AppBar";
import CssBaseline from "@material-ui/core/CssBaseline";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import { CreditCard, Grid as GridIcon, LogOut } from "react-feather";
import Gateways from "../Gateways/Gateways";
import Transactions from "../Transactions/Transactions";
import * as H from "history";
import { IState, User } from "../../store/reducer";
import { Dispatch } from "redux";
import { ThunkDispatch } from "redux-thunk";
import { AppAction, getUser } from "../../store/actionCreators";
import { connect } from "react-redux";

const drawerWidth = 240;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
    },
    drawerPaper: {
      width: drawerWidth,
    },
    drawerContainer: {
      overflow: "auto",
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
    large: {
      width: theme.spacing(7),
      height: theme.spacing(7),
    },
    title: {
      flexGrow: 1,
    },
    toolbarButton: {
      color: "#fff",
    },
  })
);

interface AppStateProps {
  user: User;
}

interface AppFunctionsProps {
  getUser: () => void;
}

export type AppProps = AppStateProps & AppFunctionsProps;

const App: React.FC<AppProps> = (props: AppProps) => {
  const classes = useStyles();
  const history: H.History = useHistory();
  const [selectedIndex, setSelectedIndex] = useState<number>(0);

  useEffect(() => {
    props.getUser();
  }, []);

  const handleGoTransactions = (): void => {
    history.push("transactions");
    setSelectedIndex(0);
  };

  const handleGoGateways = (): void => {
    history.push("gateways");
    setSelectedIndex(1);
  };

  const handleGoMenu = (): void => {
    window.location.href = "/";
  };

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <Typography variant="h6" className={classes.title}>
            Panel de Administración
          </Typography>
          <Button
            className={classes.toolbarButton}
            onClick={() => (window.location.href = "/logout")}
          >
            Log Out
          </Button>
        </Toolbar>
      </AppBar>
      <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <Toolbar />
        <div className={classes.drawerContainer}>
          <Box mt={3} mb={2}>
            <Grid
              direction="column"
              container
              alignItems="center"
              justify="center"
            >
              <Grid item>
                <Avatar
                  src={props.user.picture}
                  alt={props.user.displayName}
                  className={classes.large}
                />
              </Grid>
              <Grid item>
                <Typography variant="h5">{props.user.displayName}</Typography>
              </Grid>
            </Grid>
          </Box>
          <Divider />
          <List>
            <ListItem
              selected={selectedIndex === 0}
              button
              onClick={() => handleGoTransactions()}
            >
              <ListItemIcon>
                <GridIcon />
              </ListItemIcon>
              <ListItemText primary="Transacciones" />
            </ListItem>
            <ListItem
              selected={selectedIndex === 1}
              button
              onClick={() => handleGoGateways()}
            >
              <ListItemIcon>
                <CreditCard />
              </ListItemIcon>
              <ListItemText primary="Gateways" />
            </ListItem>
            <Divider />
            <ListItem button onClick={() => handleGoMenu()}>
              <ListItemIcon>
                <LogOut />
              </ListItemIcon>
              <ListItemText primary="Regresar al menú" />
            </ListItem>
          </List>
        </div>
      </Drawer>
      <main className={classes.content}>
        <Toolbar />
        <Switch>
          <Route path="/admin/transactions" exact component={Transactions} />
          <Route path="/admin/gateways" exact component={Gateways} />
        </Switch>
      </main>
    </div>
  );
};

const mapStateToProps: (state: IState) => AppStateProps = (
  state: IState
): AppStateProps => ({
  user: state.user,
});

const mapDispatchToProps: (dispatch: Dispatch) => AppFunctionsProps = (
  dispatch: ThunkDispatch<IState, undefined, AppAction>
): AppFunctionsProps => ({
  getUser: (): void => dispatch(getUser()),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
