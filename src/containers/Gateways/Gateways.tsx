import React, { useEffect, useState } from "react";
import { Box, Button, Container, Grid, Typography } from "@material-ui/core";
import Divider from "@material-ui/core/Divider";
import { Catalog, Gateway, IState } from "../../store/reducer";
import { Dispatch } from "redux";
import { ThunkDispatch } from "redux-thunk";
import {
  AppAction,
  createGateway,
  deleteGateway,
  getGateways,
  getProviders,
  setGatewayLoading,
  updateGateway,
} from "../../store/actionCreators";
import { connect } from "react-redux";
import { GatewaysTable } from "../../components/GatewaysTable/GatewaysTable";
import { SkeletonTable } from "../../components/Skeleton/SkeletonTable";
import { DeleteDialog } from "../../components/Dialog/DeleteDialog";
import { Plus } from "react-feather";
import { GatewaysDialog } from "../../components/Dialog/GatewaysDialog";

interface GatewaysStateProps {
  gateways: Gateway[];
  providers: Catalog[];
  gatewayLoading: boolean;
}

interface GatewaysFunctionsProps {
  getGateways: () => void;
  getProviders: () => void;
  deleteGateway: (id: string) => void;
  updateGateway: (gateway: Gateway) => void;
  createGateway: (gateway: Gateway) => void;
  setGatewayLoading: (payload: boolean) => AppAction;
}

export type GatewaysProps = GatewaysStateProps & GatewaysFunctionsProps;

const Gateways: React.FC<GatewaysProps> = (props: GatewaysProps) => {
  const [gatewayId, setGatewayId] = useState<string>("");
  const [selectedGateway, setSelectedGateway] = useState<Gateway | undefined>(
    undefined
  );
  const [openDeleteModal, setOpenDeleteModal] = useState<boolean>(false);
  const [openGatewaysModal, setOpenGatewaysModal] = useState<boolean>(false);

  useEffect(() => {
    props.setGatewayLoading(true);
    props.getGateways();
    props.getProviders();
  }, []);

  const handleDeleteDialogOpen = (id: string): void => {
    setGatewayId(id);
    setOpenDeleteModal(true);
  };

  const handleDeleteClose = (modalSelection: string): void => {
    const id: string = gatewayId;

    setGatewayId("");
    setOpenDeleteModal(false);

    if (modalSelection !== "delete") return;

    props.setGatewayLoading(true);
    props.deleteGateway(id);
  };

  const handleGatewaysClose = (
    modalSelection: string,
    gateway: Gateway,
    action: "create" | "update"
  ): void => {
    setOpenGatewaysModal(false);
    if (modalSelection !== "save") return;

    const new_gateway = { ...gateway };

    if (action === "update") {
      setSelectedGateway(undefined);

      props.updateGateway(gateway);

      return;
    }

    props.createGateway(new_gateway);
  };

  const handleUpdateGateway = (gateway: Gateway): void => {
    setSelectedGateway({ ...gateway });
    setOpenGatewaysModal(true);
  };

  const handleChangeGatewayStatus = (
    gateway: Gateway,
    newStatus: string
  ): void => {
    props.updateGateway({
      ...gateway,
      status: newStatus,
    });
  };

  const handleCreateGateway = (): void => {
    setSelectedGateway(undefined);
    setOpenGatewaysModal(true);
  };

  return (
    <React.Fragment>
      <Container fixed>
        <Grid container direction="column" spacing={3}>
          <Grid item container spacing={1}>
            <Grid
              item
              container
              alignItems="center"
              justify="flex-start"
              direction="row"
            >
              <Typography color={"primary"} variant="h1">
                Gateways
              </Typography>
            </Grid>
          </Grid>
          <Divider />
          <Box mt={2} />
          <Grid item container spacing={1}>
            <Grid
              item
              xs={12}
              container
              alignItems="center"
              justify="flex-end"
              direction="row"
            >
              <Button
                variant="contained"
                color="primary"
                disabled={props.gatewayLoading}
                onClick={handleCreateGateway}
                size={"large"}
                startIcon={<Plus />}
              >
                Crear Gateway
              </Button>
            </Grid>
          </Grid>
          <Grid item>
            {props.gatewayLoading ? (
              <SkeletonTable headCells={6} bodyCells={7} rows={5} />
            ) : (
              <GatewaysTable
                gateways={props.gateways}
                onDelete={handleDeleteDialogOpen}
                onUpdate={handleUpdateGateway}
                onChangeStatus={handleChangeGatewayStatus}
              />
            )}
          </Grid>
        </Grid>
      </Container>
      <DeleteDialog open={openDeleteModal} onClose={handleDeleteClose} />
      <GatewaysDialog
        open={openGatewaysModal}
        onClose={handleGatewaysClose}
        gateway={selectedGateway}
        providers={props.providers}
      />
    </React.Fragment>
  );
};

const mapStateToProps: (state: IState) => GatewaysStateProps = (
  state: IState
): GatewaysStateProps => ({
  gateways: state.gateways,
  providers: state.providers,
  gatewayLoading: state.gatewayLoading,
});

const mapDispatchToProps: (dispatch: Dispatch) => GatewaysFunctionsProps = (
  dispatch: ThunkDispatch<IState, undefined, AppAction>
): GatewaysFunctionsProps => ({
  getGateways: (): void => dispatch(getGateways()),
  getProviders: (): void => dispatch(getProviders()),
  setGatewayLoading: (payload: boolean): AppAction =>
    dispatch(setGatewayLoading(payload)),
  deleteGateway: (id: string): void => dispatch(deleteGateway(id)),
  updateGateway: (gateway: Gateway): void => dispatch(updateGateway(gateway)),
  createGateway: (gateway: Gateway): void => dispatch(createGateway(gateway)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Gateways);
