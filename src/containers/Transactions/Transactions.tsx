import React, { useEffect, useState } from "react";
import {
  Box,
  Container,
  Divider,
  FormControl,
  Grid,
  IconButton,
  InputAdornment,
  InputLabel,
  OutlinedInput,
  TextField,
  Typography,
} from "@material-ui/core";
import {
  DateRangePicker,
  DateRange,
  DateRangeDelimiter,
} from "@material-ui/pickers";
import { Search, X } from "react-feather";
import { TransactionsDialog } from "../../components/Dialog/TransactionsDialog";
import { connect } from "react-redux";
import { IState, Transaction } from "../../store/reducer";
import { Dispatch } from "redux";
import { ThunkDispatch } from "redux-thunk";
import {
  AppAction,
  getTransactionById,
  getTransactions,
  setTransactionLoading,
} from "../../store/actionCreators";
import { SkeletonTable } from "../../components/Skeleton/SkeletonTable";
import { TransactionsTable } from "../../components/TransactionsTable/TransactionsTable";
import { ListPagination } from "../../components/ListPagination/ListPagination";
import { add } from "date-fns";

interface TransactionsStateProps {
  transactions: Transaction[];
  transactionsLoading: boolean;
  hideNext: boolean;
  page: number;
}

interface TransactionsFunctionsProps {
  getTransactions: (
    payload: number,
    lowerDate?: number,
    upperDate?: number
  ) => void;
  getTransactionById: (payload: string) => void;
  setTransactionLoading: (payload: boolean) => AppAction;
}

export type TransactionsProps = TransactionsStateProps &
  TransactionsFunctionsProps;

const Transactions: React.FC<TransactionsProps> = (
  props: TransactionsProps
) => {
  const [value, setValue] = React.useState<DateRange<Date>>([null, null]);
  const [searchText, setSearchText] = React.useState<string>("");
  const [isSearch, setIsSearch] = React.useState<boolean>(false);
  const [selectedIndex, setSelectedIndex] = useState<number>(-1);
  const [openExitModal, setOpenExistModal] = useState<boolean>(false);

  useEffect(() => {
    props.setTransactionLoading(true);
    props.getTransactions(props.page);
  }, []);

  useEffect(() => {
    const [lower, upper] = value;

    if (Boolean(lower) && Boolean(upper)) {
      props.setTransactionLoading(true);
      props.getTransactions(
        1,
        lower!.getTime(),
        add(upper!.getTime(), { days: 1 }).getTime()
      );
    }
  }, [value]);

  const handleSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchText(event.target.value);
  };

  const handleSearchClick = () => {
    setIsSearch(!isSearch);
    props.setTransactionLoading(true);

    if (isSearch) {
      setSearchText("");
      props.getTransactions(1);
    } else {
      props.getTransactionById(searchText);
    }
  };

  const handleClearDate = () => {
    const [lower, upper] = value;

    if (Boolean(lower) && Boolean(upper)) {
      setValue([null, null]);
      props.setTransactionLoading(true);
      props.getTransactions(1);
    }
  };

  const handleCloseModal = (): void => {
    setOpenExistModal(false);

    setSelectedIndex(-1);
  };

  const handleChangePage = (page: number): void => {
    props.setTransactionLoading(true);
    props.getTransactions(page);
  };

  return (
    <React.Fragment>
      <Container fixed>
        <Grid container direction="column" spacing={3}>
          <Grid item container spacing={1}>
            <Grid
              item
              container
              alignItems="center"
              justify="flex-start"
              direction="row"
            >
              <Typography color={"primary"} variant="h1">
                Transacciones
              </Typography>
            </Grid>
          </Grid>
          <Divider />
          <Box mt={2} />
          <Grid item container spacing={1} alignItems="center">
            <Grid
              item
              xs={6}
              container
              alignItems="center"
              justify="flex-start"
              direction="row"
            >
              <DateRangePicker
                startText="Inicio"
                endText="Fin"
                value={value}
                disableFuture
                onChange={(newValue) => setValue(newValue)}
                renderInput={(startProps, endProps) => (
                  <React.Fragment>
                    <TextField {...startProps} />
                    <DateRangeDelimiter> - </DateRangeDelimiter>
                    <TextField {...endProps} />
                  </React.Fragment>
                )}
              />
              <IconButton onClick={handleClearDate}>
                <X size={15} />
              </IconButton>
            </Grid>
            <Grid item xs={6} justify="flex-end" container direction="row">
              <FormControl size="small" variant="outlined">
                <InputLabel htmlFor="search-input">Buscar por ID</InputLabel>
                <OutlinedInput
                  id="search-input"
                  value={searchText}
                  onChange={handleSearchChange}
                  endAdornment={
                    <InputAdornment position="end">
                      <IconButton onClick={handleSearchClick} edge="end">
                        {isSearch ? <X /> : <Search />}
                      </IconButton>
                    </InputAdornment>
                  }
                  labelWidth={70}
                />
              </FormControl>
            </Grid>
          </Grid>
          <Grid item>
            {props.transactionsLoading ? (
              <SkeletonTable headCells={6} bodyCells={7} rows={5} />
            ) : (
              <React.Fragment>
                <TransactionsTable
                  transactions={props.transactions}
                  selectedIndex={selectedIndex}
                  setOpenExistModal={setOpenExistModal}
                  setSelectedIndex={setSelectedIndex}
                />
                <ListPagination
                  disableNext={props.hideNext}
                  disablePrevious={props.page === 1}
                  handleChangePage={handleChangePage}
                  page={props.page}
                />
              </React.Fragment>
            )}
          </Grid>
        </Grid>
      </Container>
      <TransactionsDialog
        open={openExitModal}
        onClose={handleCloseModal}
        transaction={props.transactions[selectedIndex]}
      />
    </React.Fragment>
  );
};

const mapStateToProps: (state: IState) => TransactionsStateProps = (
  state: IState
): TransactionsStateProps => ({
  transactions: state.transactions,
  transactionsLoading: state.transactionLoading,
  hideNext: state.hideNext,
  page: state.page,
});

const mapDispatchToProps: (dispatch: Dispatch) => TransactionsFunctionsProps = (
  dispatch: ThunkDispatch<IState, undefined, AppAction>
): TransactionsFunctionsProps => ({
  getTransactions: (
    payload: number,
    lowerDate?: number,
    upperDate?: number
  ): void => dispatch(getTransactions(payload, lowerDate, upperDate)),
  getTransactionById: (payload: string): void =>
    dispatch(getTransactionById(payload)),
  setTransactionLoading: (payload: boolean): AppAction =>
    dispatch(setTransactionLoading(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Transactions);
