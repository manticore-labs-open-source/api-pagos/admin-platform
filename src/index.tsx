import React from "react";
import ReactDOM from "react-dom";
import App from "./containers/App/App";
import { environment } from "./environments/environment";
import { BrowserRouter } from "react-router-dom";
import { ThemeProvider } from "@material-ui/core/styles";
import theme from "./theme";
import { LocalizationProvider } from "@material-ui/pickers";
import DateFnsUtils from "@material-ui/pickers/adapter/date-fns";
import esLocale from "date-fns/locale/es";
import { Provider } from "react-redux";
import { applyMiddleware, compose, createStore, Store } from "redux";
import { reducer } from "./store/reducer";
import thunk from "redux-thunk";

const composeEnhancers = environment.devTools
  ? (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
  : compose;

const store: Store = createStore(
  reducer,
  composeEnhancers(applyMiddleware(thunk))
);

ReactDOM.render(
  <React.Fragment>
    <LocalizationProvider dateAdapter={DateFnsUtils} locale={esLocale}>
      <ThemeProvider theme={theme}>
        <Provider store={store}>
          <BrowserRouter>
            <App />
          </BrowserRouter>
        </Provider>
      </ThemeProvider>
    </LocalizationProvider>
  </React.Fragment>,
  document.getElementById("root")
);
