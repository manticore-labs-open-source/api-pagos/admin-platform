import { ActionTypes } from "./actionTypes";
import { Catalog, Gateway, IState, Transaction, User } from "./reducer";
import { ThunkAction, ThunkDispatch } from "redux-thunk";
import axios, { AxiosResponse } from "axios";
import { environment } from "../environments/environment";

export interface AppAction {
  type: string;
  gateways?: Gateway[];
  providers?: Catalog[];
  transactions?: Transaction[];
  gatewayLoading?: boolean;
  transactionLoading?: boolean;
  user?: User;
  hideNext?: boolean;
  page?: number;
}

export const setGateways = (payload: Gateway[]): AppAction => {
  return {
    type: ActionTypes.SET_GATEWAYS,
    gateways: payload,
  };
};

export const setProviders = (payload: Catalog[]): AppAction => {
  return {
    type: ActionTypes.SET_PROVIDERS,
    providers: payload,
  };
};

export const setTransactions = (payload: Transaction[]): AppAction => {
  return {
    type: ActionTypes.SET_TRANSACTIONS,
    transactions: payload,
  };
};

export const setGatewayLoading = (payload: boolean): AppAction => {
  return {
    type: ActionTypes.SET_GATEWAY_LOADING,
    gatewayLoading: payload,
  };
};

export const setTransactionLoading = (payload: boolean): AppAction => {
  return {
    type: ActionTypes.SET_TRANSACTION_LOADING,
    transactionLoading: payload,
  };
};

export const setUser = (payload: User): AppAction => {
  return {
    type: ActionTypes.SET_USER,
    user: payload,
  };
};

export const setHideNext = (payload: boolean): AppAction => {
  return {
    type: ActionTypes.SET_HIDE_NEXT,
    hideNext: payload,
  };
};

export const setPage = (payload: number): AppAction => {
  return {
    type: ActionTypes.SET_PAGE,
    page: payload,
  };
};

export const getGateways = (): ThunkAction<
  void,
  IState,
  undefined,
  AppAction
> => {
  return async (
    dispatch: ThunkDispatch<IState, any, AppAction>
  ): Promise<void> => {
    const axios_response: AxiosResponse<Gateway[]> = await axios.get(
      `${environment.baseUrl}gateways`
    );

    dispatch(setGateways(axios_response.data));
    dispatch(setGatewayLoading(false));
  };
};

export const getTransactions = (
  page: number,
  lowerDate?: number,
  upperDate?: number
): ThunkAction<void, IState, undefined, AppAction> => {
  return async (
    dispatch: ThunkDispatch<IState, any, AppAction>
  ): Promise<void> => {
    dispatch(setHideNext(false));
    dispatch(setPage(page));

    let url = `${environment.baseUrl}card/transaction?page=${page}`;

    if (Boolean(lowerDate) && Boolean(upperDate))
      url = `${environment.baseUrl}card/transaction?page=${page}&upperDate=${upperDate}&lowerDate=${lowerDate}`;

    let axios_response: AxiosResponse<Transaction[]> = await axios.get(url);

    if (axios_response.data.length === 0) {
      dispatch(setHideNext(true));

      if (page !== 1) {
        dispatch(setPage(page - 1));
        axios_response = await axios.get(
          `${environment.baseUrl}card/transaction?page=${page - 1}`
        );
      }
    }
    dispatch(setTransactions(axios_response.data));
    dispatch(setTransactionLoading(false));
  };
};

export const getTransactionById = (
  id: string
): ThunkAction<void, IState, undefined, AppAction> => {
  return async (
    dispatch: ThunkDispatch<IState, any, AppAction>
  ): Promise<void> => {
    dispatch(setHideNext(true));
    let transactions: Transaction[] = [];

    try {
      const axios_response: AxiosResponse<Transaction> = await axios.get(
        `${environment.baseUrl}card/transaction/${id}`
      );

      transactions = [axios_response.data];
    } catch (e) {
      console.log(e);
    }

    dispatch(setTransactions(transactions));
    dispatch(setTransactionLoading(false));
  };
};

export const getUser = (): ThunkAction<void, IState, undefined, AppAction> => {
  return async (
    dispatch: ThunkDispatch<IState, any, AppAction>
  ): Promise<void> => {
    const axios_response: AxiosResponse<User> = await axios.get(
      `${environment.baseUrl}user`
    );

    dispatch(setUser(axios_response.data));
  };
};

export const getProviders = (): ThunkAction<
  void,
  IState,
  undefined,
  AppAction
> => {
  return async (
    dispatch: ThunkDispatch<IState, any, AppAction>
  ): Promise<void> => {
    const axios_response: AxiosResponse<Catalog[]> = await axios.get(
      `${environment.baseUrl}gateways/providers`
    );

    dispatch(setProviders(axios_response.data));
  };
};

export const deleteGateway = (
  id: string
): ThunkAction<void, IState, undefined, AppAction> => {
  return async (
    dispatch: ThunkDispatch<IState, any, AppAction>
  ): Promise<void> => {
    await axios.delete(`${environment.baseUrl}gateways/${id}`);

    dispatch(getGateways());
  };
};

export const updateGateway = (
  gateway: Gateway
): ThunkAction<void, IState, undefined, AppAction> => {
  return async (
    dispatch: ThunkDispatch<IState, any, AppAction>
  ): Promise<void> => {
    await axios.patch(`${environment.baseUrl}gateways/${gateway.id}`, {
      name: gateway.name,
      credential: {
        publicCredential: gateway.credential.publicCredential,
        privateCredential: gateway.credential.privateCredential,
      },
      status: gateway.status,
    });

    dispatch(getGateways());
  };
};

export const createGateway = (
  gateway: Gateway
): ThunkAction<void, IState, undefined, AppAction> => {
  return async (
    dispatch: ThunkDispatch<IState, any, AppAction>
  ): Promise<void> => {
    await axios.post(`${environment.baseUrl}gateways`, {
      name: gateway.name,
      credential: {
        publicCredential: gateway.credential.publicCredential,
        privateCredential: gateway.credential.privateCredential,
      },
      type: gateway.type,
      provider: gateway.provider,
    });

    dispatch(getGateways());
  };
};
