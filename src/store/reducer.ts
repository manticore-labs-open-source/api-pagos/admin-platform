import { ActionTypes } from "./actionTypes";
import { AppAction } from "./actionCreators";

export interface Catalog {
  value: string;
  name: string;
}

export interface Gateway {
  id: string;
  created: number;
  name: string;
  credential: GatewayCredential;
  type: string;
  status: string;
  provider: string;
}

export interface GatewayCredential {
  publicCredential: string;
  privateCredential: string;
}

export interface Transaction {
  id: string;
  created: number;
  token: Record<string, any>;
  amount: number;
  type: string;
  status: string;
  responseCode: string;
  responseText: string;
  description: string;
  currency: string;
  gatewayId: string;
  metadata: Record<string, any>;
  details: Record<string, any>;
  transactionReference: string;
  provider: string;
}

export interface User {
  picture: string;
  displayName: string;
}

export interface IState {
  gateways: Gateway[];
  providers: Catalog[];
  transactions: Transaction[];
  gatewayLoading: boolean;
  transactionLoading: boolean;
  hideNext: boolean;
  user: User;
  page: number;
}

export const INITIAL_STATE: IState = {
  gateways: [],
  providers: [],
  transactions: [],
  gatewayLoading: false,
  transactionLoading: false,
  user: {
    picture: "",
    displayName: "",
  },
  hideNext: false,
  page: 1,
};

export const reducer = (
  state: IState = INITIAL_STATE,
  action: AppAction
): IState => {
  switch (action.type) {
    case ActionTypes.SET_GATEWAYS:
      return {
        ...state,
        gateways: action.gateways!,
      };
    case ActionTypes.SET_PROVIDERS:
      return {
        ...state,
        providers: action.providers!,
      };
    case ActionTypes.SET_GATEWAY_LOADING:
      return {
        ...state,
        gatewayLoading: action.gatewayLoading!,
      };
    case ActionTypes.SET_TRANSACTION_LOADING:
      return {
        ...state,
        transactionLoading: action.transactionLoading!,
      };
    case ActionTypes.SET_USER:
      return {
        ...state,
        user: action.user!,
      };
    case ActionTypes.SET_TRANSACTIONS:
      return {
        ...state,
        transactions: action.transactions!,
      };
    case ActionTypes.SET_HIDE_NEXT:
      return {
        ...state,
        hideNext: action.hideNext!,
      };
    case ActionTypes.SET_PAGE:
      return {
        ...state,
        page: action.page!,
      };
    default:
      return state;
  }
};
