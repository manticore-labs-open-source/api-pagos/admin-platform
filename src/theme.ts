/* istanbul ignore file */
// @ts-nocheck
import { createMuiTheme } from "@material-ui/core/styles";

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "rgba(2,51,101,1)",
      dark: "rgba(17,43,69,1)",
      light: "rgba(30,101,174,1)",
    },
  },
  typography: {
    fontFamily: "IBM Plex Sans,sans-serif",
    h1: {
      fontSize: "40px",
      fontWeight: 200,
      lineHeight: "140%",
      fontStyle: "normal",
      color: "#023365",
    },
    h2: {
      fontSize: "32px",
      fontWeight: 500,
      lineHeight: "140%",
      fontStyle: "normal",
    },
    h3: {
      fontSize: "28px",
      fontWeight: 500,
      lineHeight: "140%",
      fontStyle: "normal",
    },
    h4: {
      fontSize: "24px",
      fontWeight: 500,
      lineHeight: "140%",
      fontStyle: "normal",
    },
    h5: {
      fontSize: "20px",
      fontWeight: 500,
      lineHeight: "140%",
      fontStyle: "normal",
    },
    h6: {
      fontSize: "16px",
      fontWeight: 400,
      lineHeight: "140%",
      fontStyle: "normal",
    },
    subtitle2: {
      fontSize: "12px",
      fontWeight: 350,
    },
  },
  overrides: {
    MuiTableContainer: {
      root: {
        backgroundColor: "#FFF",
      },
    },
    MuiTableRow: {
      hover: {
        "&:hover": {
          backgroundColor: "#F7FAFC !important",
        },
        "&$selected": {
          backgroundColor: "rgba(213, 234, 255, 0.4) !important",
        },
      },
    },
    MuiTableCell: {
      head: {
        fontStyle: "normal",
        fontWeight: 500,
        fontSize: "14px",
        lineHeight: "140%",
        color: "#677784",
      },
    },
    MuiButton: {
      root: {
        textTransform: "none",
      },
      outlined: {
        textTransform: "none",
      },
    },
  },
});

export default theme;
